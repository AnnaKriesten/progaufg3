/**
 * Exponential.java
 * input: a real x, a natural number n and an accuracy eps
 * output: an approximation of exp(x)
 *
 * method: two methods are implemented.
 * if n>0 and eps=0, the approximation equals the nth partial sum of the exponential series
 * if n=0 and eps>0, the approximation equals the kth partial sum, where k is such that the
 * difference between the kth and the (k-1)st partial sum is not larger than eps
 *        
 */
import java.util.Scanner;

/**
 * Calculates the exponential function exp(x) using an approximation by the
 * formula sum_{i=1}^n x^i/i! for n to infinity. It can handle both, fixed n
 * and approximation to a given accuracy.
 */
public class Exponential{

	/**
	 * read input and output solution
	 */
	public static void main(String[] args){
		
		double eApprox = 2.7183;
		
	    // number of iterations		
	    int n;
	    
		// accuracy
	    double eps;
	    
	    // the argument for the exponential series
	    double x;

	    //prompt input from user		
	    System.out.println("Bitte geben Sie ein Funktionsargument x, eine nat�rliche Zahl n, und eine Genauigkeit eps ein");
	    System.out.println("Ist eps=0 und n>0, so wird die n-te Partialsumme der Exponentialreihe ausgeben");
	    System.out.println("Ist n=0 und eps>0, so wird die k-te Partialsumme der Exponentialreihe ausgeben, wobei k so gew�hlt ist,"
					   + " dass die Differenz der k-ten und (k-1)-ten Partialsumme kleiner gleich eps ist.");
	    
	    // use scanner class to read input
	    Scanner scan = new Scanner(System.in);
	    x = scan.nextDouble();
	    n = scan.nextInt();
	    eps = scan.nextDouble();
	    
	    if ( n > 0 && eps == 0 ) {
	    	System.out.println("expSum(x,n) = " + expSum(x,n));	
		}
		else if (n == 0 && eps > 0 ) {
			System.out.println("Die Approximation ist " + expApproximate(x, eps));
		}
	    // cases in which the input is not reasonable
		else { //if ( (n > 0 && eps > 0) || (n < 0 || eps < 0)){
	    	System.out.println("Keine sinnvollen Eingabeparamter! Ueberpruefen Sie die Eingabe!");
	    }

	    // exercise part (e)
	    System.out.println("\nZur Approximation von e auf mind. 4 Nachkommastellen genau (e = " + eApprox + ") sollte n mindestens zu " + minimal(1, eApprox) +" gewaehlt werden.");
	    System.out.println("F�r x = 3.1416 erh�lt man f�r n = " + minimal(1, eApprox) + ": e^x = " + expSum(3.1416, minimal(1, eApprox)) + ".");
	    scan.close();	    
	}

	/**
	 * Evaluates the value sum_{i=1}^n x^i/i!
	 * @param n the number of summations used by the approximation
	 * @param x the value for which exp(x) should be computed
	 * @return the value of the first $n$ sum elements of exp(x)
	 */
	public static double expSum( double x, int n ) {
		double sum = 0;
		
		// the loop is not entered for n<0
		for (int i = 0; i <= n; i++){
			sum += power(x,i)/factorial(i);
		}
	    return sum;
	}

	/**
	 * Approximates the value of e^x via x_k = sum_{i=1}^n x^i/i! until |x_k - x_k+1| < eps
	 * @param x
	 * @param eps
	 * @return the approximation of e^x
	 */
	public static double expApproximate( double x, double eps ) {
	    double difference = 1;
	    double approximation = 0;
	    double sumI = expSum(x,0);
	    double sumIplus = expSum(x,1);
		for (int i = 2; difference > eps; i++){
			//Calculate difference and build absolute value
			difference = Math.abs(sumIplus - sumI);
			sumI = sumIplus;
			sumIplus = expSum(x,i);
	   	}
		return approximation;
	}

	/**
	 * Computes the value base to the exp.
	 * @param base the basis
	 * @param exp the exponent
	 * @return the value base to the exp
	 */
	public static double power( double base, int exp ) {
		double result = 1;
		
		// for exp == 0, this loop is not entered
		for( int i = 0; i < exp; i++ ){
			result *= base;
		}
		return result;	
	}

	/**
	 * Calculates the faculty of a given number.
	 * @param n the number
	 * @return the faculty of a given number
	 */
	public static long factorial( int n ) {
		long result = 1;

		// for n == 0 or n == 1, the loop is not entered
		for (int i = 2; i <= n; i++){
			result *= i;			
		}				
		return result;
	}

	/**
	 * Calculation of minimum n for reaching the accuracy of the value compare when calculating exp(x,n)  
	 * @param x
	 * @param compare
	 * @return the minimum n or 0 (if n > 100 or the compare value is wrong)
	 */
	public static int minimal(double x, double compare){
		double minimalValue = 0;
		for (int i = 0; i < 100 ; i++){
			minimalValue = expSum(x,i);
			if (Math.round(minimalValue*10000)/10000.0 == compare){
				return i;
			}	
		}
		return 0;
	}
}